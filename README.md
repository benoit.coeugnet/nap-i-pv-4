# NAS

## Présentation

Projet de config BGP/MPLS VPN pour avoir une bonne note en réseau. Dans l'ensemble, ce script permet une automatisation d'une configuration réseau BGP/MPLS VPN. Ce script contient une interface graphique qui permet de gérer la configuration des routeurs : leurs adresses, leurs voisins, leur métrique ospf etc.

## Avant de commencer

Avant de lancer le projet, il faut nécessairement installer la library style, afin de faire fonctionner l'interface graphique :

    pip install style


Pour lancer le projet :

    python main_interface.py

## Description de l'interface graphique

![interface](Interface.png "Interface graphique")

### Partie gauche

Pour chaque routeur, on doit rentrer dans l'ordre : 
- son nom (clé primaire)
- son AS
- son adresse de loopback
- son aire OSPF
- les VRF dans lesquels il est impliqué

Le bouton "Add a neighbor" sert à ajouter un voisin au routeur.

Les voisins s'affichent en dessous. Les information dans l'ordre sont :
- nom du routeur voisin
- interface où il est connecté
- métrique OSPF 

### Partie droite

Pour générer la config pour la première fois, utiliser le bouton "Full config".

Si on modifie uniquement certains routeurs, utiliser "Only updated".

Le bouton "Add a router" sert à ajouter un nouveau routeur à la config.