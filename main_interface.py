import xml.etree.ElementTree as Et
import tkinter
import tkinter.ttk as ttk
from init import main, init, modify

#Document config reading
tree = Et.parse('conf.xml')
root = tree.getroot()

class Neighbor:
    def __init__(self, frame, neighbor_line, name, interface, routerParent, metric):
        self.frame = frame
        self.ligne = neighbor_line
        self.name = name
        self.interface = interface
        self.routerParent = routerParent
        self.metric = metric
        self.create_neighbor(neighbor_line)

    def create_neighbor(self, line):
        if self.name != -1:
            name = tkinter.StringVar()
            name.set(self.name)
            self.ligne +=1
        else:
            name = tkinter.StringVar()
            name.set("Neighbor " + str(self.ligne))
            self.name = str(name.get())

        b0 = tkinter.Entry(self.frame, textvariable=name, width=15)

        interfaceValues = ["fastEth", "Eth1", "Eth2", "Eth3"]
        b1 = ttk.Combobox(self.frame, values=interfaceValues, width=10)
        if self.interface != -1:
            b1.current(self.interface)
        else:
            b1.current(0)
            self.interface = 0

        b2 = ttk.Button(self.frame, text="X", command=self.delete_neighbor, width=3)
        b3 = ttk.Button(self.frame, text="V", width=3, command=self.modify_intent)
        ospfMetric = tkinter.StringVar()
        ospfMetric.set(self.metric)
        b4 = tkinter.Entry(self.frame, textvariable=ospfMetric, width=10)
        b0.grid(row=self.ligne, column=0)
        b1.grid(row=self.ligne, column=1)
        b2.grid(row=self.ligne, column=3)
        b3.grid(row=self.ligne, column=4)
        b4.grid(row=self.ligne, column=2)
        frame_buttons.update_idletasks()
        canvas.config(scrollregion=canvas.bbox("all"))

    def delete_neighbor(self):
        for router in root.findall('routers'):
            if router.get('name') == self.routerParent:
                for neighbor in router.findall('neighbor'):
                    if neighbor.get('name') == self.name:
                        router.remove(neighbor)
                        tree.write('conf.xml')
                        if self.name not in routerToUpdate:
                            routerToUpdate.append(self.name)
                            print(routerToUpdate)
                        print("Neighbor", neighbor.get('name'), "deleted from the config file")
        self.frame.destroy()
        self.ligne -= 1
        frame_buttons.update_idletasks()
        canvas.config(scrollregion=canvas.bbox("all"))

    # Adding a neighbor to the config file for this router
    def modify_intent(self):
        for router in root.findall('routers'):
            if router.get('name') == self.routerParent:
                for neighbor in router.findall('neighbor'):
                    # Update the neighbor if it already exists
                    if neighbor.get('name') == self.name:
                        if self.name in routerToUpdate:
                            routerToUpdate.remove(self.name)
                        self.name = self.frame.winfo_children()[0].get()
                        self.interface = self.frame.winfo_children()[1].current()
                        self.metric = self.frame.winfo_children()[4].get()
                        neighbor.set('name', self.name)
                        neighbor.set('interface', str(self.interface))
                        neighbor.set('metric', self.metric)
                        tree.write('conf.xml')
                        print("Neighbor", neighbor.get('name'), "updated in the config file")
                        frame_buttons.update_idletasks()
                        canvas.config(scrollregion=canvas.bbox("all"))
                        if self.name not in routerToUpdate:
                            routerToUpdate.append(self.name)
                            print(routerToUpdate)
                        return
                self.name = self.frame.winfo_children()[0].get()
                self.interface = self.frame.winfo_children()[1].current()
                self.metric = self.frame.winfo_children()[4].get()
                newNeighbor = Et.SubElement(router, 'neighbor', name=str(self.name), interface=str(self.interface), metric=str(self.metric))
                print("Neighbor", newNeighbor.get('name'), "added to the config file")
                tree.write('conf.xml')
                frame_buttons.update_idletasks()
                canvas.config(scrollregion=canvas.bbox("all"))
                if self.name not in routerToUpdate:
                    routerToUpdate.append(self.name)
                    print(routerToUpdate)

class Intent:
    def __init__(self, frame, router_ligne, nameOfThisRouter, asOfThisRouter, loopbackOfThisRouter, ospfArea, vrfTunnel):
        self.frame = frame
        self.router_ligne = router_ligne
        self.j = 1
        self.nameOfThisRouter = nameOfThisRouter
        self.asOfThisRouter = asOfThisRouter
        self.loopbackOfThisRouter = loopbackOfThisRouter
        self.ospfArea = ospfArea
        self.vrfTunnel = vrfTunnel
        self.creation()


    def creation(self):
        nameOfThisRouter = tkinter.StringVar()
        nameOfThisRouter.set(self.nameOfThisRouter)
        asOfThisRouter = tkinter.StringVar()
        asOfThisRouter.set(self.asOfThisRouter)
        loopbackOfThisRouter = tkinter.StringVar()
        loopbackOfThisRouter.set(self.loopbackOfThisRouter)
        ospfArea = tkinter.StringVar()
        ospfArea.set(self.ospfArea)
        vrfTunnel = tkinter.StringVar()
        vrfTunnel.set(self.vrfTunnel)
        nameOfThisRouter.trace("w", self.modify_intent)
        e0 = tkinter.Entry(self.frame, textvariable=nameOfThisRouter, width=15)
        e1 = tkinter.Entry(self.frame, textvariable=asOfThisRouter, width=10)
        e2 = tkinter.Entry(self.frame, textvariable=loopbackOfThisRouter, width=15)
        e3 = tkinter.Entry(self.frame, textvariable=ospfArea, width=10)
        e4 = tkinter.Entry(self.frame, textvariable=vrfTunnel, width=10)
        b0 = ttk.Button(self.frame, text="Add a neighbor", command=self.addNeighbor)
        b1 = ttk.Button(self.frame, text="X", width=3, command=self.delete_frame)
        b2 = ttk.Button(self.frame, text="V", width=3, command=self.modify_intent)
        e0.grid(row=0, column=0)
        e1.grid(row=0, column=1)
        e2.grid(row=0, column=2)
        e3.grid(row=0, column=3)
        e4.grid(row=0, column=4)
        b0.grid(row=0, column=5)
        b1.grid(row=0, column=6)
        b2.grid(row=0, column=7)


    def addNeighbor(self):
        i = tkinter.Frame()
        i.grid(columnspan=5, in_ = self.frame)
        Neighbor(i, self.j, -1, -1, self.nameOfThisRouter, 0)
        self.j += 1

    def delete_frame(self):
        global nbRouterOnTheConfigFile
        for router in root.findall('routers'):
            if router.get('name') == self.nameOfThisRouter:
                root.remove(router)
                tree.write('conf.xml')
                if self.nameOfThisRouter not in routerToUpdate:
                    routerToUpdate.append(self.nameOfThisRouter)
                    print(routerToUpdate)
                print("Router", router.get('name'), "deleted from the config file")
        self.frame.destroy()
        nbRouterOnTheConfigFile -= 1
        frame_buttons.update_idletasks()
        canvas.config(scrollregion=canvas.bbox("all"))

    def modify_intent(self):
        for router in root.findall('routers'):
            # Updating the name and AS of the router if already in the config file
            if router.get('name') == self.nameOfThisRouter:
                if self.nameOfThisRouter in routerToUpdate:
                    routerToUpdate.remove(self.nameOfThisRouter)
                self.nameOfThisRouter = self.frame.winfo_children()[0].get()
                self.asOfThisRouter = self.frame.winfo_children()[1].get()
                self.loopbackOfThisRouter = self.frame.winfo_children()[2].get()
                self.ospfArea = self.frame.winfo_children()[3].get()
                self.vrfTunnel = self.frame.winfo_children()[4].get()
                router.set('name', self.nameOfThisRouter)
                router.set('AS', self.asOfThisRouter)
                router.set('loopback', self.loopbackOfThisRouter)
                router.set('ospfArea', self.ospfArea) 
                router.set('vrfTunnel', self.vrfTunnel)
                print("Router", router.get('name'), "updated in the config file")
                tree.write('conf.xml')
                frame_buttons.update_idletasks()
                canvas.config(scrollregion=canvas.bbox("all"))
                if self.nameOfThisRouter not in routerToUpdate:
                    routerToUpdate.append(self.nameOfThisRouter)
                    print(routerToUpdate)
                return
        self.nameOfThisRouter = self.frame.winfo_children()[0].get()
        self.asOfThisRouter = self.frame.winfo_children()[1].get()
        self.loopbackOfThisRouter = self.frame.winfo_children()[2].get()
        self.ospfArea = self.frame.winfo_children()[3].get()
        self.vrfTunnel = self.frame.winfo_children()[4].get()
        newRouter = Et.SubElement(root, 'routers', name=self.nameOfThisRouter, AS=self.asOfThisRouter, loopback=self.loopbackOfThisRouter, area=self.ospfArea, vrfTunnel=self.vrfTunnel)
        tree.write('conf.xml')
        print("Router", newRouter.get('name'), "added to the config file")
        frame_buttons.update_idletasks()
        canvas.config(scrollregion=canvas.bbox("all"))
        if self.nameOfThisRouter not in routerToUpdate:
            routerToUpdate.append(self.nameOfThisRouter)
            print(routerToUpdate)


def new_router():
    global nbRouterOnTheConfigFile
    global configurationRouter
    h = tkinter.Frame(frame_buttons)
    h.grid()
    nbRouterOnTheConfigFile += 1
    nameOfThisRouter = "Router " + str(nbRouterOnTheConfigFile)
    asOfThisRouter = "AS here"
    configurationRouter.append(Intent(h, nbRouterOnTheConfigFile, nameOfThisRouter, asOfThisRouter, "loopback", "area", "vrfTunnel"))
    frame_buttons.update_idletasks()
    canvas.config(scrollregion=canvas.bbox("all"))


rootTk = tkinter.Tk()
nbRouterOnTheConfigFile = 0
rootTk.title("Network topology generator")
rootTk.geometry("825x600")
rootTk.resizable(width=False, height=False)

canvas = tkinter.Canvas(rootTk)
canvas.grid(row=0, column=0, sticky="news")

vsb = ttk.Scrollbar(rootTk, orient="vertical", command=canvas.yview)
vsb.grid(row=0, column=1, sticky='ns')
canvas.configure(yscrollcommand=vsb.set)

frame_buttons = ttk.Frame(canvas)
canvas.create_window((0, 0), window=frame_buttons, anchor='nw')

global configurationRouter
global configurationNeighborhood
global routerToUpdate
routerToUpdate = []
configurationRouter = []
configurationNeighborhood = [[]]

for router in root.findall('routers'):
    f = tkinter.Frame(frame_buttons)
    f.grid(row=nbRouterOnTheConfigFile, column=0)
    configurationRouter.append(Intent(f, nbRouterOnTheConfigFile + 1, router.get('name'), router.get('AS'), router.get('loopback'), router.get('ospfArea'), router.get('vrfTunnel')))
    nbRouterOnTheConfigFile += 1
    nbNeighborForThisRouter = 0
    for neighbor in router.findall('neighbor'):
        i = tkinter.Frame()
        i.grid(columnspan=5, in_=f)
        configurationNeighborhood[nbRouterOnTheConfigFile - 1].append(Neighbor(i, nbNeighborForThisRouter, neighbor.get('name'), neighbor.get('interface'), router.get('name'), neighbor.get('metric')))
        nbNeighborForThisRouter += 1
    configurationNeighborhood.append([])

line = nbRouterOnTheConfigFile - 1

#put all routers name in a list
listOfRouters = []
for router in root.findall('routers'):
    listOfRouters.append(router.get('name'))
bFC = ttk.Button(rootTk, text="Full config", command=lambda:main(listOfRouters)) #Button to launch the program with all routers
bFC.grid(row=0, column=2)
bLa = ttk.Button(rootTk, text="Only updated", command=lambda:main(routerToUpdate)) #Button to launch the program with only updated routers
bLa.grid(row=0, column=3)
bAdd = ttk.Button(rootTk, text="Add a router", command=new_router)
bAdd.grid(row=0, column=4)


frame_buttons.update_idletasks()
canvas.config(scrollregion=canvas.bbox("all"), width=550, height=550)

rootTk.mainloop()