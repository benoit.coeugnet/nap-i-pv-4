import xml.etree.ElementTree as Et
import os
import style as style

#Document config reading
tree = Et.parse('conf.xml')
root = tree.getroot()

# List of routers
networkTopology = []

global nbRouterOnTheConfigFile

# -------------------------------------------------- Declaration of class --------------------------------------------------

#Class to define each router and these attributes
class Routers:
    def __init__(self, name, neighbors, protocol, name_of_as, border, border_neighbors, nb_ports, loopback, ospfArea, mask, vrfTunnel):
        self.name = name
        self.neighborsList = neighbors
        self.protocol = protocol
        self.AS = name_of_as
        self.border = border
        self.bn = border_neighbors
        self.ports = [0] * nb_ports
        self.addressing = [0] * nb_ports
        self.metric = [0] * nb_ports
        self.loopback = loopback
        self.ospfArea = ospfArea
        self.mask = mask
        self.vrfTunnel = vrfTunnel
        self.vrfNeighbor = []
        
    # def __repr__(self):
        # return f"{self.name} {self.neighborsList} {self.protocol} {self.AS} " \
        #     f"{self.border} {self.bn} {self.ports} {self.addressing} {self.metric} " \
        #     f"{self.loopback} {self.mask} {self.ospfArea} {self.vrfTunnel} {self.vrfNeighbor}"
        

# To get the index of the end of the network prefix
def get_network_prefix(address):
    prefix = address.split("/")[0]
    return prefix

def mask_maker(mask_number):
    res = ""
    for i in range(4):
        if mask_number >= 8:
            res += "255."
            mask_number -= 8
        elif 0 < mask_number < 8 :
            res += str(256 - pow(2, 8 - mask_number)) + "."
            mask_number = 0
        else :
            res += "0."
    return res[:-1]

def get_router_on_the_same_as(router_name):
    router_iter = 0
    while networkTopology[router_iter].name != router_name:
        router_iter += 1
    thisAS = networkTopology[router_iter].AS
    inTheSameAS = []
    for router in networkTopology:
        if router.AS == thisAS and router.name != router_name:
            inTheSameAS.append(router)
    return inTheSameAS

def main(routerToUpdate):
    #Document config reading
    tree = Et.parse('conf.xml')
    root = tree.getroot()
    print(routerToUpdate)
    networkTopology = init(root)
    while routerToUpdate != []:
        modify(networkTopology, routerToUpdate.pop(), root)

# -------------------------------------------------- Initialisation --------------------------------------------------

def init(root):
    # Define an object for each router with his name and neighbors
    networkTopology = []
    i = 0
    for routers in root.findall('routers'):
        networkTopology.append(Routers('', [], '', '', False, [], 4, "", 0, 0, []))
        networkTopology[i].name = routers.get('name')
        networkTopology[i].AS = routers.get('AS')
        networkTopology[i].loopback = routers.get("loopback")
        networkTopology[i].vrfTunnel = routers.get("vrfTunnel").split(",")
        
        # List the neighbors of this router and the interface
        for neighbor in routers.iter('neighbor'):
            networkTopology[i].neighborsList.append(neighbor.get('name'))
            networkTopology[i].ports[int(neighbor.get('interface'))] = neighbor.get('name')
            if neighbor.get('metric') is not None:
                networkTopology[i].metric[int(neighbor.get('interface'))] = neighbor.get('metric')
        i += 1

    # Define for each router, the AS and protocol
    for autonomousSystem in root.findall('AutonomousSys'):

        nameOfThisAS = autonomousSystem.get('name')
        protocolInThisAS = autonomousSystem.find('protocol').text
        coupleOfRoutersNumber = 1

        # Find the network prefix
        addressingInThisAS = autonomousSystem.find('addressing').text 
        maskInThisAS = mask_maker(int(addressingInThisAS.split("/")[1]))
        addressingInThisAS = get_network_prefix(addressingInThisAS) 
        print(addressingInThisAS)

        # Set the protocol of each route
        for i in range(len(networkTopology)):
            if nameOfThisAS == networkTopology[i].AS:
                networkTopology[i].protocol = protocolInThisAS
                networkTopology[i].mask = maskInThisAS

        # Put the address of each interface
        for routerIter in range(len(networkTopology)):
            for neighborIter in range(4):
                if (networkTopology[routerIter].ports[neighborIter] != 0) and (networkTopology[routerIter].addressing[neighborIter] == 0):
                    # Find the index of the neighbor
                    for i in range(len(networkTopology)):
                        if networkTopology[i].name == networkTopology[routerIter].ports[neighborIter]:
                            indexNeighbor = i
                    # Only put the address on router on the same AS
                    if networkTopology[indexNeighbor].AS == networkTopology[routerIter].AS:
                        networkTopology[routerIter].addressing[neighborIter] = str(addressingInThisAS) + str(coupleOfRoutersNumber) + ".1"
                        n = networkTopology[indexNeighbor].ports.index(networkTopology[routerIter].name)
                        networkTopology[indexNeighbor].addressing[n] = str(addressingInThisAS) + str(coupleOfRoutersNumber) + ".2" 
                        coupleOfRoutersNumber += 1
                    # If they are a vrfTunnel in common, put the address
                    elif networkTopology[indexNeighbor].name[:2] == "CE":
                        if networkTopology[indexNeighbor].vrfTunnel[0] in networkTopology[routerIter].vrfTunnel:
                            # Find the address of the AS of the CE router
                            for autonomousSystem in root.findall('AutonomousSys'):
                                if autonomousSystem.get('name') == networkTopology[indexNeighbor].AS:
                                    addressingInThisVRF = autonomousSystem.find('addressing').text
                                    addressingInThisVRF = get_network_prefix(addressingInThisVRF)
                            networkTopology[routerIter].addressing[neighborIter] = str(addressingInThisVRF) + "2"
                            n = networkTopology[indexNeighbor].ports.index(networkTopology[routerIter].name)
                            networkTopology[indexNeighbor].addressing[n] = str(addressingInThisVRF) + "1"
                            # Put the routers in the vrfNeighbor list
                            networkTopology[routerIter].vrfNeighbor.append(networkTopology[indexNeighbor].name)
                            networkTopology[indexNeighbor].vrfNeighbor.append(networkTopology[routerIter].name)

    # Define if a router is a border router and these borders neighbors
    for i in range(len(networkTopology)):
        #print(networkTopology[i].neighborsList, type(networkTopology[i].neighborsList))
        for j in range(len(networkTopology[i].neighborsList)):
            k = 0
            while networkTopology[i].neighborsList[j] != networkTopology[k].name:
                k += 1
            if networkTopology[k].AS != networkTopology[i].AS:
                networkTopology[i].border = True
                networkTopology[i].bn.append(networkTopology[i].neighborsList[j])


    for i in range(len(networkTopology)):
        print(1)
    #    print(networkTopology[i])
    #   print(get_router_on_the_same_as("R9"))
    return networkTopology

# -------------------------------------------------- Modifying files --------------------------------------------------
    
def modify(networkTopology, updateThisRouter, root):
    # Creation du dictionnaire avec folder + num de routeur associe
    folder_path = "project-files/dynamips"

    # for i in range(len(networkTopology)):
    
    i = 0
    find = False
    for routers in root.findall('routers'):
        if routers.get('name') != updateThisRouter and not find:
            i += 1
        else:
            find = True

    # Setting up file names
    nomFichier = "i"+str(i+1)+"_startup-config.cfg"
    print("Updating "+nomFichier)

    # Opening files
    fichier = open(nomFichier, "w")
    default1 = open("Default1.txt","r")
    default2 = open("Default2.txt","r")
    default2bis = open("Default2-bis.txt","r")
    default3 = open("Default3.txt","r")
    default4 = open("Default4.txt","r")

    # Add lines before host
    for line in default1:
        fichier.write(line)

    # Add hostname
    print("hostname "+networkTopology[i].name+"\n")
    fichier.write("\nhostname "+networkTopology[i].name+"\n")

    # Add everything else before interfaces
    if networkTopology[i].name[:1] == 'P':
        for line in default2:
            fichier.write(line)
    else:
        for line in default2bis:
            fichier.write(line)
        
        
    
    for num in range(1,3):
        toWriteVRFConf = ""
        if networkTopology[i].name[:2] == 'PE':
            facile = str(num) + ":" + str(num) + "\n"
            toWriteVRFConf = "\nip vrf vrfce" + networkTopology[i].vrfTunnel[num - 1] + "\n rd " + facile
            toWriteVRFConf += " route-target export " + facile
            toWriteVRFConf += " route-target import " + facile
            fichier.write(toWriteVRFConf + "!")



    #Add Loopback
    toWriteInterfaces = ""
    toWriteInterfaces += "\ninterface Loopback1\n"
    toWriteInterfaces += " ip address " + str(networkTopology[i].loopback) + " 255.255.255.255\n"
    if networkTopology[i].protocol == "OSPF":
        toWriteInterfaces += " ip ospf 1 area " + str(networkTopology[i].ospfArea) + "\n"
    # toWriteInterfaces += "ip ospf " + "1" + " area " + str(networkTopology[i].ospfArea) + "\n!"
    fichier.write(toWriteInterfaces + "!")


    # Add Interfaces
    toWriteInterfaces = ""
    for j in range(4):
        
        if j == 0: # If Interface is Fast
            toWriteInterfaces += "\ninterface FastEthernet0/0\n"
        else: # If Interface is Giga
            toWriteInterfaces += "interface GigabitEthernet"+str(j)+"/0\n"

        # If interface unused
        if networkTopology[i].addressing[j] == 0:
            toWriteInterfaces += " no ip address\n shutdown\n"
            if j == 0:
                toWriteInterfaces += " duplex full\n!\n"
            else:
                toWriteInterfaces += " negotiation auto\n!\n"

        # If interface used
        else:
            np = 0
            if networkTopology[i].ports[j] in networkTopology[i].vrfNeighbor and networkTopology[i].name[:2] == "PE":
                toWriteInterfaces += " ip vrf forwarding vrfce" + str(networkTopology[i].ports[j][-1:]) + "\n"
                np = 1
            toWriteInterfaces += " ip address " + str(networkTopology[i].addressing[j]) + " " + str(networkTopology[i].mask)
            if networkTopology[i].name[:1] == "P" and np == 0:
                toWriteInterfaces += "\n ip ospf " + str(networkTopology[i].metric[j]) + " area " + str(networkTopology[i].ospfArea)
                toWriteInterfaces += "\n negotiation auto\n mpls ip\n!\n"
            else:
                toWriteInterfaces += "\n negotiation auto\n!\n"

    fichier.write(toWriteInterfaces)
    listeVoisins = networkTopology[i].neighborsList
    
    
    
    # BGP
    if networkTopology[i].name[:2] == "PE":
        fichier.write("!\nrouter bgp "+str(networkTopology[i].AS)+"\n")
        for k in range(len(networkTopology)):
            if networkTopology[k].name[:2] == "PE" and networkTopology[k].name != networkTopology[i].name:
                neighborAddress1 = ""
                fichier.write(" bgp log-neighbor-changes\n")
                for l in range (len(networkTopology[k].ports)): # To find the interface between the 2 neighbors
                    neighborAddress1 = networkTopology[k].loopback
                        
                fichier.write(" neighbor " + neighborAddress1 + " remote-as " + str(networkTopology[i].AS) + "\n")
                fichier.write(" neighbor " + neighborAddress1 + " update-source Loopback1 \n")
                """ fichier.write(" no bgp default ipv4-unicast\n") """

    thereIsSthToDoWithBGP = False
    Done = False
    # Add neighbors in other AS
    for j in range (len(listeVoisins)): # Parcours de la liste des voisins de i
        for k in range(len(networkTopology)):
            # Only if the neighbor hasn't a name which begin with "CE"
            if networkTopology[k].name[:2] != "CE":
                if listeVoisins[j] == networkTopology[k].name: # Les voisins de i ont l'indice k
                    if networkTopology[i].AS != networkTopology[k].AS: # If neighbor is in a different AS
                        neighborAddress = ""
                        for l in range (len(networkTopology[k].ports)): # To find the interface between the 2 neighbors
                            if networkTopology[k].ports[l] == networkTopology[i].name:
                                neighborAddress = str(networkTopology[k].addressing[l])
                                thereIsSthToDoWithBGP = True
                        if thereIsSthToDoWithBGP and not Done:
                            fichier.write("!\nrouter bgp " + str(networkTopology[i].AS) + "\n")
                            fichier.write(" bgp log-neighbor-changes\n")
                            Done = True
                        fichier.write(" neighbor "+neighborAddress)
                        fichier.write(" remote-as " +str(networkTopology[k].AS)+"\n")

    if networkTopology[i].name[:2] == "PE" or networkTopology[i].name[:2] == "CE":
        fichier.write(" !\n address-family ipv4\n")
        for j in range(len(listeVoisins)):
            for k in range(len(networkTopology)):
                if listeVoisins[j] == networkTopology[k].name: # Les voisins de i ont l'indice k
                    # If neighbor is in a different AS
                    for l in range (len(networkTopology[k].ports)): # To find the interface between the 2 neighbors
                        if (networkTopology[k].ports[l] == networkTopology[i].name) and networkTopology[k].name[:2] != "CE":
                            neighborAddress2 = str(networkTopology[k].addressing[l])
                            # If the router is a CE router
                            if networkTopology[i].name[:2] == "CE":
                                fichier.write("  network " + neighborAddress2[:len(neighborAddress2) - 1] + "0" + "\n")
                                fichier.write("  redistribute connected" + "\n")
                            if networkTopology[i].name[:2] == "PE":
                                fichier.write("  neighbor " + neighborAddress1 + " activate\n")
                            else:
                                fichier.write("  neighbor " + neighborAddress2 + " activate\n")
        fichier.write(" exit-address-family\n!\n") 
        """ fichier.write(" address-family ipv6\n") """
        ###VPNv4 family routes

        # Only if the name of the router begins with "PE"
        if networkTopology[i].name[:2] == "PE":
            fichier.write(" address-family vpnv4\n")
            for k in range(len(networkTopology)):
                if networkTopology[k].name[:2] == "PE" and networkTopology[k].name != networkTopology[i].name:
                    # neighborAddress1 = ""
                    # for l in range(len(networkTopology[k].ports)):  # To find the interface between the 2 neighbors
                    #     neighborAddress1 = networkTopology[k].addressing[1]
                    fichier.write("  neighbor " + neighborAddress1 +" activate\n")
                    fichier.write("  neighbor " + neighborAddress1 + " send-community extended\n")
            fichier.write(" exit-address-family\n")
            
        if networkTopology[i].name[:2] == "PE":
            for l in networkTopology[i].vrfTunnel:
                fichier.write(" !\n address-family ipv4 vrf vrfce" + str(l) + "\n  redistribute connected\n  ")
                for voisin in listeVoisins:
                    for router in networkTopology:
                        if router.name == voisin and router.name[:2] == "CE" and router.vrfTunnel[0] == l:
                            neighborAddress3 = str(router.addressing[router.ports.index(networkTopology[i].name)])
                            fichier.write("neighbor "+ neighborAddress3 + " remote-as " + router.AS + "\n")
                            fichier.write("  neighbor " + neighborAddress3 + " activate\n")
                fichier.write(" exit-address-family\n")

    # Add thing before redistribute connected
    for line in default3:
        fichier.write(line)

    # Add redistribute connected or router-id
    """ if networkTopology[i].protocol == "RIP":
        fichier.write("\nrouter rip AS"+str(networkTopology[i].AS)+"\n redistribute connected\n") """
    if networkTopology[i].protocol == "OSPF":
        fichier.write("\nrouter ospf 1\n router-id "+str(i+1)+"."+str(i+1)+"."+str(i+1)+"."+str(i+1)+"\n")
    else :
        fichier.write("\n")

    # Add everything before line con 0
    for line in default4:
        fichier.write(line)


    # Closing files
    fichier.close()
    default1.close()
    default2.close()
    default3.close()
    default4.close()

# Put files in good folder
for i in range(len(networkTopology)):
    print(4)
    for path, dirs, files in os.walk(folder_path):
        print(2)
        for filename in files: # Pour tous les fichiers dans dynamips
            print(3)
            nameToHave = "i"+str(i+1)+"_startup"
            if nameToHave in filename: # Pour tous les fichiers startup-config
                os.rename(filename,path+"/"+filename)
                print("Moved "+filename+" to "+path+"/"+filename)

